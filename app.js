const express = require('express');
const app = express();
require('dotenv').config()
// Endpoint untuk menyediakan nilai variabel lingkungan
app.get('/api/env', (req, res) => {
    const envVariables = {
        RESULT: process.env.RESULT,
        CLIENT_ID: process.env.CLIENT_ID,
        CLIENT_SECRET: process.env.CLIENT_SECRET,
        TOKEN_HOST: process.env.TOKEN_HOST,
        FR: process.env.FR
    };

    res.json(envVariables);
});
// Port yang akan digunakan oleh server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});